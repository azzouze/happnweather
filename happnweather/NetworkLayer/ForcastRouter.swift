//
//  UserRouter.swift
//  happnweather
//
//  Created by Abdelaziz Halaouet on 9/25/19.
//  Copyright © 2019 Abdelaziz Halaouet. All rights reserved.
//


import Foundation
import Alamofire


enum ForcastRouter: URLRequestConvertible {
    
    case get(CityKey : Int)
    
    var method: HTTPMethod {
        switch self {
        case .get:
            return .get
        }
    }
    
    var parameters: [String : Any]? {
        switch self {
        case .get:
            return ["language":"fr","metric":"true","apikey":Constants.apiKey]
        }
    }
    
    
    
    var url: URL {
        let relativePath : String?
        switch self {
        case .get(let CityKey):
            relativePath = Constants.forecastEndpoint+"\(CityKey)"
        }
        
        var url = URL(string: Constants.baseURL)!
        if let relativePath = relativePath {
            url = url.appendingPathComponent(relativePath)
        }
        return url
    }
    
    var encoding: ParameterEncoding {
        return URLEncoding.default
    }
    
    func asURLRequest() throws -> URLRequest {
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = method.rawValue
        return try encoding.encode(urlRequest, with: parameters)
    }
}

