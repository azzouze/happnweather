//
//  Constants.swift
//  happnweather
//
//  Created by Abdelaziz Halaouet on 9/25/19.
//  Copyright © 2019 Abdelaziz Halaouet. All rights reserved.
//


import Foundation

struct Constants {
    
    static let baseURL = "http://dataservice.accuweather.com/forecasts/v1/"
    static let forecastEndpoint = "daily/5day/"
    static let apiKey = "6pvXtJlr50PIazHfacgRl2xkL9429ZxK"
    static let iconURL = "https://developer.accuweather.com/sites/default/files/"
}
