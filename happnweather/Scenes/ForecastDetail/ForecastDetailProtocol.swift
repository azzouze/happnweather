//
//  UsersProtocol.swift
//  happnweather
//
//  Created by Abdelaziz Halaouet on 9/25/19.
//  Copyright © 2019 Abdelaziz Halaouet. All rights reserved.
//

import Foundation

protocol ForecastDetailViewProtocol: class {
    var presenter: ForecastDetailPresenterProtocol! { get set }
    func showLoadingIndicator()
    func hideLoadingIndicator()
    func loadData(viewModel : ForecastDetailViewModel)
}

protocol ForecastDetailPresenterProtocol: class {
    var view: ForecastDetailViewProtocol? { get set }
    func viewDidLoad()
    func dismiss()
}

protocol ForecastDetailRouterProtocol {
    func navigateBackToListViewController(from view: ForecastDetailViewProtocol)
}

protocol ForecastDetailInteractorInputProtocol {
    var presenter: ForecastDetailInteractorOutputProtocol? { get set }
    var viewModel : ForecastDetailViewModel! { get set }
    
}

protocol ForecastDetailInteractorOutputProtocol: class {
   
}

protocol ForecastDetailView {
    func configure(viewModel: ForecastViewModel)
}
