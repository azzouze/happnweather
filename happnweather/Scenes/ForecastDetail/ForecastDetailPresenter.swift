//
//  UsersPresenter.swift
//  happnweather
//
//  Created by Abdelaziz Halaouet on 9/25/19.
//  Copyright © 2019 Abdelaziz Halaouet. All rights reserved.
//

import Foundation

class ForecastDetailPresenter: ForecastDetailPresenterProtocol, ForecastDetailInteractorOutputProtocol {
    
    weak var view: ForecastDetailViewProtocol?
    private let interactor: ForecastDetailInteractorInputProtocol
    private let router: ForecastDetailRouterProtocol
    private var forecasts = [Forecast]()
    private var cityKey : Int = 623
    
    var numberOfRows: Int {
        return forecasts.count
    }
    
    init(view: ForecastDetailViewProtocol, interactor: ForecastDetailInteractorInputProtocol, router: ForecastDetailRouterProtocol) {
        self.view = view
        self.interactor = interactor
        self.router = router
    }
    
    func viewDidLoad() {
        view?.showLoadingIndicator()
        view?.loadData(viewModel : interactor.viewModel)
    }
    
   
    func dismiss() {
        router.navigateBackToListViewController(from: view!)
    }
}
