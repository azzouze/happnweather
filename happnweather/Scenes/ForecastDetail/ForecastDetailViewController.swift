//
//  UsersViewController.swift
//  happnweather
//
//  Created by Abdelaziz Halaouet on 9/25/19.
//  Copyright © 2019 Abdelaziz Halaouet. All rights reserved.
//

import UIKit
import Kingfisher

class ForecastDetailViewController: UIViewController, ForecastDetailViewProtocol {
    
    var presenter: ForecastDetailPresenterProtocol!
    
    @IBOutlet weak var minTempLabel : UILabel!
    @IBOutlet weak var maxTempLabel : UILabel!
    @IBOutlet weak var joursIcon : UIImageView!
    @IBOutlet weak var joursDesc : UILabel!
    @IBOutlet weak var nuitIcon : UIImageView!
    @IBOutlet weak var nuitDesc : UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.viewDidLoad()
        
    }
    
    func tapToDismissConfigure(){
        let tapGestureRec = UITapGestureRecognizer.init(target: self, action: #selector(dismissVC))
        self.view.addGestureRecognizer(tapGestureRec)
    }
    
    func showLoadingIndicator() {
        print("Should show loading indicator")
    }
    
    func hideLoadingIndicator() {
        print("Should hide loading indicator")
    }
    
    func loadData(viewModel: ForecastDetailViewModel) {
        minTempLabel.text = "\(viewModel.minTemperature)°"
        maxTempLabel.text = "\(viewModel.maxTemperature)°"
        joursDesc.text = viewModel.dayWeather
        joursIcon.kf.setImage(with : viewModel.dayImage)
        nuitDesc.text = viewModel.nightWeather
        nuitIcon.kf.setImage(with : viewModel.nightImage)
        
    }
    
    @IBAction func closeAction(){
        self.dismissVC()
    }
    
    @objc func dismissVC(){
        self.presenter.dismiss()
    }
    
}
