//
//  UsersRouter.swift
//  happnweather
//
//  Created by Abdelaziz Halaouet on 9/25/19.
//  Copyright © 2019 Abdelaziz Halaouet. All rights reserved.
//

import UIKit

class ForecastDetailRouter: ForecastDetailRouterProtocol {
    
    weak var viewController: UIViewController?
    
    static func createModule( viewModel : ForecastDetailViewModel) -> UIViewController {
        let view = UIStoryboard(name: "ForecastDetail", bundle: nil).instantiateViewController(withIdentifier: "\(ForecastDetailViewController.self)") as! ForecastDetailViewController
        let interactor = ForecastDetailInteractor()
        interactor.viewModel = viewModel
        let router = ForecastDetailRouter()
        let presenter = ForecastDetailPresenter(view: view, interactor: interactor, router: router)
        view.presenter = presenter
        interactor.presenter = presenter
        router.viewController = view
        return view
    }
    
    func navigateBackToListViewController(from view: ForecastDetailViewProtocol) {
        guard let viewVC = view as? UIViewController else {
            fatalError("Invalid view protocol type")
        }
        viewVC.dismiss(animated: true, completion: nil)
    }
    
}
