//
//  UsersViewController.swift
//  happnweather
//
//  Created by Abdelaziz Halaouet on 9/25/19.
//  Copyright © 2019 Abdelaziz Halaouet. All rights reserved.
//

import UIKit

class ForecastListViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, ForecastListViewProtocol {
    
    var presenter: ForecastListPresenterProtocol!
    
    @IBOutlet weak var forecastsTableView: UITableView!
    @IBOutlet weak var forecastsDescription: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTableView()
        presenter.viewDidLoad()
    }
    
    private func setupTableView() {
        forecastsTableView.dataSource = self
        forecastsTableView.delegate = self
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.numberOfRows
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "forecastCell", for: indexPath) as! ForecastCell
        presenter.configure(cell: cell, indexPath: indexPath)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        presenter.showDetail(ofIndex: indexPath.row)
    }
    
    func showLoadingIndicator() {
        print("Should show loading indicator")
    }
    
    func hideLoadingIndicator() {
        print("Should hide loading indicator")
    }
    
    func reloadData(description : String) {
        forecastsDescription.text = description
        forecastsTableView.reloadData()
    }
    
}
