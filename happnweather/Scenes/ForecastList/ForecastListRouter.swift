//
//  UsersRouter.swift
//  happnweather
//
//  Created by Abdelaziz Halaouet on 9/25/19.
//  Copyright © 2019 Abdelaziz Halaouet. All rights reserved.
//

import UIKit

class ForecastListRouter: ForecastListRouterProtocol {
    
    weak var viewController: UIViewController?
    
    static func createModule() -> UIViewController {
        let view = UIStoryboard(name: "ForecastList", bundle: nil).instantiateViewController(withIdentifier: "\(ForecastListViewController.self)") as! ForecastListViewController
        let interactor = ForecastListInteractor()
        let router = ForecastListRouter()
        let presenter = ForecastListPresenter(view: view, interactor: interactor, router: router)
        view.presenter = presenter
        interactor.presenter = presenter
        router.viewController = view
        return view
    }
    
    func presentDetailScreen(from view: ForecastListViewProtocol, for forecast: ForecastDetailViewModel) {
        
        let forecastDetailVC = ForecastDetailRouter.createModule(viewModel: forecast)
        
        guard let viewVC = view as? UIViewController else {
            fatalError("Invalid View Protocol type")
        }
        forecastDetailVC.modalPresentationStyle = .overCurrentContext
        
        viewVC.present(forecastDetailVC, animated: true, completion: nil)
    }
    
}
