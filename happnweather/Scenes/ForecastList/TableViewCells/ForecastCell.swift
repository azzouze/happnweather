//
//  UserCell.swift
//  VIPER
//
//  Created by Vortex on 4/7/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit
import Kingfisher

class ForecastCell: UITableViewCell, ForecastCellView {
    
    @IBOutlet weak var weatherImageView: UIImageView!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
        setupImageView()
    }
    
    private func setupImageView() {
        weatherImageView.layer.cornerRadius = weatherImageView.frame.height / 2
    }
    
    func configure(viewModel: ForecastViewModel) {
        weatherImageView.kf.setImage(with : viewModel.image)
        descriptionLabel.text = viewModel.weather
        dateLabel.text = viewModel.day
    }
    
}
