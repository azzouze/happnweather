//
//  Interactor.swift
//  happnweather
//
//  Created by Abdelaziz Halaouet on 9/25/19.
//  Copyright © 2019 Abdelaziz Halaouet. All rights reserved.
//

import Foundation

class ForecastListInteractor: ForecastListInteractorInputProtocol {
    
    weak var presenter: ForecastListInteractorOutputProtocol?
    
    private let forcastWorker = DailyForcastWorker()
    var weatherDescription = ""
    func getForcasts(CityKey : Int) {
        forcastWorker.getForcasts(CityKey: CityKey, completionHandler: { [weak self] result in
            guard let self = self else { return }
            switch result {
            case .success(let forecasts):
                self.presenter?.forecastsFetchedSuccessfully(forecasts: forecasts)
            case .failure(let error):
                self.presenter?.forecastsFetchingFailed(withError: error)
            }
        })
    }
    
}
