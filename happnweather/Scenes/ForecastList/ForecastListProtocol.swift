//
//  UsersProtocol.swift
//  happnweather
//
//  Created by Abdelaziz Halaouet on 9/25/19.
//  Copyright © 2019 Abdelaziz Halaouet. All rights reserved.
//

import Foundation

protocol ForecastListViewProtocol: class {
    var presenter: ForecastListPresenterProtocol! { get set }
    func showLoadingIndicator()
    func hideLoadingIndicator()
    func reloadData(description : String)
}

protocol ForecastListPresenterProtocol: class {
    var view: ForecastListViewProtocol? { get set }
    var numberOfRows: Int { get }
    func viewDidLoad()
    func configure(cell: ForecastCellView, indexPath: IndexPath)
    func showDetail(ofIndex : Int)
}

protocol ForecastListRouterProtocol : class {
    func presentDetailScreen(from view: ForecastListViewProtocol, for forecast: ForecastDetailViewModel)
}

protocol ForecastListInteractorInputProtocol {
    var presenter: ForecastListInteractorOutputProtocol? { get set }
    func getForcasts(CityKey : Int)
    
}

protocol ForecastListInteractorOutputProtocol: class {
    func forecastsFetchedSuccessfully(forecasts: DailyForecast)
    func forecastsFetchingFailed(withError error: Error)
}

protocol ForecastCellView {
    func configure(viewModel: ForecastViewModel)
}
