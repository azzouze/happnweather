//
//  UsersPresenter.swift
//  happnweather
//
//  Created by Abdelaziz Halaouet on 9/25/19.
//  Copyright © 2019 Abdelaziz Halaouet. All rights reserved.
//

import Foundation

class ForecastListPresenter: ForecastListPresenterProtocol, ForecastListInteractorOutputProtocol {
    
    
    
    weak var view: ForecastListViewProtocol?
    private let interactor: ForecastListInteractorInputProtocol
    private let router: ForecastListRouterProtocol
    private var forecasts : DailyForecast!
    private var list = [Forecast]()
    private var cityKey : Int = 623
    
    var numberOfRows: Int {
        return list.count
    }
    
    init(view: ForecastListViewProtocol, interactor: ForecastListInteractorInputProtocol, router: ForecastListRouterProtocol) {
        self.view = view
        self.interactor = interactor
        self.router = router
    }
    
    func viewDidLoad() {
        view?.showLoadingIndicator()
        interactor.getForcasts(CityKey: cityKey)
    }
    
    func forecastsFetchedSuccessfully(forecasts: DailyForecast) {
        view?.hideLoadingIndicator()
        list = forecasts.DailyForecasts
        self.forecasts = forecasts
        view?.reloadData(description : forecasts.Headline.Text)
    }
    
    func forecastsFetchingFailed(withError error: Error) {
        view?.hideLoadingIndicator()
        //Failure - Should show alert
    }
    
    func configure(cell: ForecastCellView, indexPath: IndexPath) {
        let forecast = forecasts.DailyForecasts[indexPath.row]
        let viewModel = ForecastViewModel(forecast: forecast)
        cell.configure(viewModel: viewModel)
    }
    
    func showDetail(ofIndex: Int) {
        router.presentDetailScreen(from: self.view!, for: ForecastDetailViewModel(forecast: forecasts.DailyForecasts[ofIndex]))
    }
}
