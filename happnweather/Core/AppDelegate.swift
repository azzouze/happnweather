//
//  AppDelegate.swift
//  happnweather
//
//  Created by Abdelaziz Halaouet on 9/23/19.
//  Copyright © 2019 Abdelaziz Halaouet. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        window?.rootViewController = UINavigationController(rootViewController: ForecastListRouter.createModule())
        return true
    }

   

}

