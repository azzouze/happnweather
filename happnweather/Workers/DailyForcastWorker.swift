//
//  UsersWorker.swift
//  happnweather
//
//  Created by Abdelaziz Halaouet on 9/25/19.
//  Copyright © 2019 Abdelaziz Halaouet. All rights reserved.
//


import Foundation

class DailyForcastWorker {
    
    private let networkLayer = NetworkLayer()
    
    func getForcasts(CityKey : Int,completionHandler: @escaping (Result<DailyForecast>) -> ()) {
        networkLayer.request(ForcastRouter.get(CityKey: CityKey), decodeToType: DailyForecast.self, completionHandler: completionHandler)
    }
    
}
