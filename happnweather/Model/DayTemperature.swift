//
//  DayTemperature.swift
//  happnweather
//
//  Created by Abdelaziz Halaouet on 9/25/19.
//  Copyright © 2019 Abdelaziz Halaouet. All rights reserved.
//

import Foundation

struct DayTemperature : Decodable{
    var Minimum : Temperature
    var Maximum : Temperature
}
