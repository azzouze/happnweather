//
//  DailyForecast.swift
//  happnweather
//
//  Created by Abdelaziz Halaouet on 9/25/19.
//  Copyright © 2019 Abdelaziz Halaouet. All rights reserved.
//

import Foundation

struct DailyForecast : Decodable {
    var Headline : Headline
    var DailyForecasts: [Forecast]
}
