//
//  Weather.swift
//  happnweather
//
//  Created by Abdelaziz Halaouet on 9/25/19.
//  Copyright © 2019 Abdelaziz Halaouet. All rights reserved.
//

import Foundation

struct Weather : Decodable{
    
    var Icon: Int
    var IconPhrase: String
    var HasPrecipitation: Bool
    var PrecipitationType: String?
    var PrecipitationIntensity: String?
}
