//
//  ForeCast.swift
//  happnweather
//
//  Created by Abdelaziz Halaouet on 9/25/19.
//  Copyright © 2019 Abdelaziz Halaouet. All rights reserved.
//

import Foundation

struct Forecast : Decodable{
    var Date: String
    var EpochDate: Int
    var Temperature: DayTemperature
    var Day : Weather
    var Night : Weather
    var MobileLink : String
}

struct ForecastViewModel {
    
    var day: String
    var weather : String
    var image : URL?
    
    
    let dateFormatter: ISO8601DateFormatter = {
        let dateFormatter = ISO8601DateFormatter()
        //dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        return dateFormatter
    }()
    
    let diplayDateFormatter: DateFormatter = {
        let diplayDateFormatter = DateFormatter()
        diplayDateFormatter.dateFormat = "dd-MM-yyyy"
        return diplayDateFormatter
    }()
    
    init(forecast: Forecast) {
        image =  URL(string: "\(Constants.iconURL)\(String.init(format: "%02d", forecast.Day.Icon))-s.png")
        weather = "\(forecast.Day.IconPhrase)"
        day = "\(diplayDateFormatter.string(from: dateFormatter.date(from: forecast.Date)!))"
    }
    
}

struct ForecastDetailViewModel {
    
    var day: String
    var dayWeather : String
    var dayImage : URL?
    var nightWeather : String
    var nightImage : URL?
    var minTemperature: Double
    var maxTemperature : Double
    
    
    let dateFormatter: ISO8601DateFormatter = {
        let dateFormatter = ISO8601DateFormatter()
        //dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        return dateFormatter
    }()
    
    let diplayDateFormatter: DateFormatter = {
        let diplayDateFormatter = DateFormatter()
        diplayDateFormatter.dateFormat = "dd-MM-yyyy"
        return diplayDateFormatter
    }()
    
    init(forecast: Forecast) {
        dayImage =  URL(string: "\(Constants.iconURL)\(String.init(format: "%02d", forecast.Day.Icon))-s.png")
        dayWeather = "\(forecast.Day.IconPhrase)"
        nightImage =  URL(string: "\(Constants.iconURL)\(String.init(format: "%02d", forecast.Night.Icon))-s.png")
        nightWeather = "\(forecast.Night.IconPhrase)"
        day = "\(diplayDateFormatter.string(from: dateFormatter.date(from: forecast.Date)!))"
        minTemperature = forecast.Temperature.Minimum.Value
        maxTemperature = forecast.Temperature.Maximum.Value
        
    }
    
}
