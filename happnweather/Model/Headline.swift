//
//  Headline.swift
//  happnweather
//
//  Created by Abdelaziz Halaouet on 9/25/19.
//  Copyright © 2019 Abdelaziz Halaouet. All rights reserved.
//

import Foundation

struct Headline : Decodable {
    var Text : String
}
